const fs = require("fs");
const path = require("path");

function createDirectory(dirName, callback) {
  if (fs.existsSync(dirName)) {
    console.log('Directory exists!');
    removeAllJSONs(dirName)
  }
  else {
  fs.mkdir(dirName, callback);
}
}

function createJSON(dirName, JSONname, callback) {
  fs.writeFile(dirName + "/" + JSONname, "{Hi:Hello}", callback);
}
//  createJSON('JSONS','b.json')

function removeAllJSONs(dirName) {
  fs.readdir(dirName, (err, files) => {
    if (err) throw err;
    files.forEach((file) => fs.unlink(dirName + "/" + file, (err) => {
      if (err) throw err;
      console.log('path/file.txt was deleted');
    }));
  });
}


// removeAllJSONs('JSONS')

module.exports = { createDirectory, createJSON, removeAllJSONs };
