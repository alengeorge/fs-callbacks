const fs = require("fs");
const readline = require('readline');


const readFile = (filename, newFileName) => {
  fs.readFile(filename, "utf8", function (err, data) {
    if (err) throw err;
    console.log("Reading: " + filename);
    // console.log(typeof data)
    let filenamtxt = 'filename.txt'
    fs.appendFile(filenamtxt, '\n'+newFileName, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`\nfilename.txt updated`);
        let data1 = data.toUpperCase()
        fs.writeFile(newFileName, data1, (err) => {
          if (err) console.log(err);
          else {
            console.log( newFileName+" written successfully\n");
            let lowerCase = 'lowercase.txt'
            fs.appendFile(filenamtxt, '\n'+lowerCase, (err) => {
              if (err) {
                console.log(err);
              } else {
                console.log(`\nfilename.txt updated`);
                let data2=data.toLowerCase().split('.').join('\n')
                fs.writeFile(lowerCase, data2, (err) => {
                  if (err) console.log(err);
                  else {
                    console.log( "lowercase.txt written successfully\n");
                    let data3 = (data1+data2).split('\n').sort().join()
                    let sortedfile = 'sorted.txt'
                    fs.writeFile(sortedfile, data3, (err) => {
                      if (err) console.log(err);
                      else {
                        console.log( sortedfile+" written successfully\n");
                        fs.appendFile(filenamtxt, '\n'+sortedfile, (err) => {
                          if (err) {
                            console.log(err);
                          } else {
                            console.log(`\n${filenamtxt} updated`);
                            const rd = readline.createInterface({
                              input: fs.createReadStream(filenamtxt),
                          });
                          rd.on('line', function(line) {
                          
                              writeFile(filenamtxt,' ')
                          })
                          }
                        });
                      }
                    });
                  }
                });
          
              }
            });
          }
        })
  
      }
    });
  });
};



/*
const readFile = (filename, newFileName,callback) => {
  fs.readFile(filename, "utf8", function (err, data) {
    if (err) throw err;
    console.log("Reading: " + filename);
    // console.log(typeof data)

    callback(newFileName,data);
  });
};

*/
function updateFile(filename, data) {
  fs.appendFile(filename, '\n'+data, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log(`\n${filename} updated`);
      
    }
  });
}

function convertToUpper(filename,data) {
  updateFile("filename.txt", filename,writeFile,);
  
}

function writeFile(filename, data) {
  fs.writeFile(filename, data, (err) => {
    if (err) console.log(err);
    else {
      console.log( filename+" updated successfully\n");
      // callback('filename.txt','sorted.txt')
    }
  });
}

function convertToLower(filename,data) {
  updateFile("filename.txt", filename);
  const sentences = data.toLowerCase().split('.').join('\n')
  writeFile(filename,sentences );
  
}

function readNewFiles(newFileName,data1){

    fs.readFile(newFileName, "utf8", function (err, data2) {
        if (err) throw err;
        console.log("Reading: " + newFileName);
        // console.log(typeof data)
    
        writeFile('sorted.txt',(data1+data2).split('\n').sort().join(),updateFile);
        // updateFile('filename.txt','sorted.txt')
      });

}

function deleteFilesStored(filename){


    const rd = readline.createInterface({
        input: fs.createReadStream(filename),
    });
    rd.on('line', function(line) {
    
        writeFile(filename,' ')
    })


}

// readFile("lipsum.txt",'upperCase.txt' ,convertToUpper);
// readFile("lipsum.txt",'LowerCase.txt' ,convertToLower)
// writeFile('filename',convertToUpper(d))

// readFile('upperCase.txt','LowerCase.txt',readNewFiles)

// deleteFilesStored('filename.txt')

module.exports={readFile,convertToLower,convertToUpper}