const { ignore } = require("nodemon/lib/rules");

const createJSON = require("../problem1").createJSON;
const createDirectory = require("../problem1").createDirectory;

const removeAllJSONs = require("../problem1").removeAllJSONs;

let dirname = "JsonsFolder";

const callback_JSON = (err) => {
  if (err) throw err;
  console.log("File Created");
  createJSON(dirname, "b.json", (err)=>{
    if(err) throw err
    console.log("File Created");
    removeAllJSONs(dirname);

})
};

createDirectory(dirname, (err) => {
  if (err) throw err;
  console.log("Directory Created");
  createJSON(dirname, "a.json", callback_JSON);
  
  });


